from flask import Flask, make_response, request
import json
import types

app = Flask(__name__)


@app.route('/shopee/test', methods=['post', 'get'])
def get_ss():
    a = request.args.get('a', type=int)
    b = request.args.get('b', type=str)
    c = request.args.get('c')
    if not a or not b:
        result3 = {
            'error_code': "21",
            'error_message': "empry or wrong params",
            'reference': "111"
        }
        return make_response(json.dumps(result3))

    if isinstance(a, int) and isinstance(b, str) and not c:
        result1 = {
            'error_code': "0",
            'error_message': "success",
            'reference': "111"
        }
        return make_response(json.dumps(result1))

    result2 = {
        'error_code': "11",
        'error_message': "system error",
        'reference': "111"
    }
    return make_response(json.dumps(result2))
